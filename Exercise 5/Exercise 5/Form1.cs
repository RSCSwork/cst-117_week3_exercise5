﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise_5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }//ends form

        private void CalculateBtn_Click(object sender, EventArgs e)
        {
            int numberOfTerms;
            double pi = 4;

            //When coming into the first if statement, we check the tryparse to see if we are getting back an int
            if(int.TryParse(numberOfTermsTxtBx.Text, out numberOfTerms))
            {
                errorLbl.Text = "";
                //here in the second layer, we first ask if the number entered was 1, if it was we can return the terms right away
                if(numberOfTerms == 1)
                {
                    numberOfTermsDisplayLbl.Text = "1 term";
                }
                //for any number but 1, or at least to the maximum value that the text box can take in, we start into the equation.
                else if (numberOfTerms > 1 && numberOfTerms <= int.MaxValue)
                {
                    long denominator = 3; //the first denominator in the sequence is 3
                    bool addOrSub = false; //a boolean for helping to decide whether to add or subtract in the equation

                    numberOfTermsDisplayLbl.Text = numberOfTerms.ToString() + " terms";

                    //this for loop iterates for the total number of terms there are.
                    for (int i = 2; i <= numberOfTerms; i++)
                    {
                        //This switch statement decides whether to add or subtract based on the point in the equation. 
                        //These switch statements will add or subtract to the equation, and afterwards update the values
                        //for the denominator and change the addOrSub to true or false so the correct adding or subtracting
                        //is done. 
                        switch (addOrSub)
                        {
                            case false:
                                pi = pi - ((double)4 / denominator);
                                denominator += 2;
                                addOrSub = !addOrSub;
                                break;

                            case true:
                                pi = pi + ((double)4 / denominator);
                                denominator += 2;
                                addOrSub = !addOrSub;
                                break;

                        }//ends switch inside for loop
                    }//ends for loop
                }//ends if else for greater than 1
                piValueLbl.Text = pi.ToString();
            }//ends outer most if before else for tryparse error
            else
            {
                errorLbl.Text = "Please enter a whole number";
            }//ends try parse else
        }//ends calculate button

    }//ends class
}//ends namespace
