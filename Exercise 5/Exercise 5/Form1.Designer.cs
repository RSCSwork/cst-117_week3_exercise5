﻿namespace Exercise_5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.numberOfTermsLbl = new System.Windows.Forms.Label();
            this.numberOfTermsTxtBx = new System.Windows.Forms.TextBox();
            this.calculateBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.numberOfTermsDisplayLbl = new System.Windows.Forms.Label();
            this.equalLbl = new System.Windows.Forms.Label();
            this.piValueLbl = new System.Windows.Forms.Label();
            this.errorLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // numberOfTermsLbl
            // 
            this.numberOfTermsLbl.AutoSize = true;
            this.numberOfTermsLbl.Location = new System.Drawing.Point(10, 78);
            this.numberOfTermsLbl.Name = "numberOfTermsLbl";
            this.numberOfTermsLbl.Size = new System.Drawing.Size(127, 20);
            this.numberOfTermsLbl.TabIndex = 0;
            this.numberOfTermsLbl.Text = "Enter # of terms:";
            // 
            // numberOfTermsTxtBx
            // 
            this.numberOfTermsTxtBx.Location = new System.Drawing.Point(168, 75);
            this.numberOfTermsTxtBx.Name = "numberOfTermsTxtBx";
            this.numberOfTermsTxtBx.Size = new System.Drawing.Size(155, 26);
            this.numberOfTermsTxtBx.TabIndex = 1;
            // 
            // calculateBtn
            // 
            this.calculateBtn.Location = new System.Drawing.Point(14, 135);
            this.calculateBtn.Name = "calculateBtn";
            this.calculateBtn.Size = new System.Drawing.Size(123, 61);
            this.calculateBtn.TabIndex = 2;
            this.calculateBtn.Text = "CALCULATE";
            this.calculateBtn.UseVisualStyleBackColor = true;
            this.calculateBtn.Click += new System.EventHandler(this.CalculateBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 253);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(214, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Approximate value of pi after ";
            // 
            // numberOfTermsDisplayLbl
            // 
            this.numberOfTermsDisplayLbl.AutoSize = true;
            this.numberOfTermsDisplayLbl.Location = new System.Drawing.Point(218, 253);
            this.numberOfTermsDisplayLbl.Name = "numberOfTermsDisplayLbl";
            this.numberOfTermsDisplayLbl.Size = new System.Drawing.Size(0, 20);
            this.numberOfTermsDisplayLbl.TabIndex = 4;
            // 
            // equalLbl
            // 
            this.equalLbl.AutoSize = true;
            this.equalLbl.Location = new System.Drawing.Point(14, 304);
            this.equalLbl.Name = "equalLbl";
            this.equalLbl.Size = new System.Drawing.Size(18, 20);
            this.equalLbl.TabIndex = 5;
            this.equalLbl.Text = "=";
            // 
            // piValueLbl
            // 
            this.piValueLbl.AutoSize = true;
            this.piValueLbl.Location = new System.Drawing.Point(27, 304);
            this.piValueLbl.Name = "piValueLbl";
            this.piValueLbl.Size = new System.Drawing.Size(0, 20);
            this.piValueLbl.TabIndex = 6;
            // 
            // errorLbl
            // 
            this.errorLbl.AutoSize = true;
            this.errorLbl.Location = new System.Drawing.Point(164, 37);
            this.errorLbl.Name = "errorLbl";
            this.errorLbl.Size = new System.Drawing.Size(0, 20);
            this.errorLbl.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 450);
            this.Controls.Add(this.errorLbl);
            this.Controls.Add(this.piValueLbl);
            this.Controls.Add(this.equalLbl);
            this.Controls.Add(this.numberOfTermsDisplayLbl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.calculateBtn);
            this.Controls.Add(this.numberOfTermsTxtBx);
            this.Controls.Add(this.numberOfTermsLbl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Approximate PI";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label numberOfTermsLbl;
        private System.Windows.Forms.TextBox numberOfTermsTxtBx;
        private System.Windows.Forms.Button calculateBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label numberOfTermsDisplayLbl;
        private System.Windows.Forms.Label equalLbl;
        private System.Windows.Forms.Label piValueLbl;
        private System.Windows.Forms.Label errorLbl;
    }
}

